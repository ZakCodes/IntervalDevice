// ----------------------------------------------------------------------------
// --- Class definition, inheritance and setup
// ----------------------------------------------------------------------------

/**
    Créer une nouvelle instance de l'application.
    @constructor
    @param {int} id
    @param {any} controller
*/
function IntervalDevice(id, controller) {
    IntervalDevice.super_.call(this, id, controller);
}

inherits(IntervalDevice, AutomationModule);

_module = IntervalDevice;

// ----------------------------------------------------------------------------
// --- Module instance initialized
// ----------------------------------------------------------------------------

IntervalDevice.prototype.init = function(config) {
    IntervalDevice.super_.prototype.init.call(this, config);

    const self = this;

    const defaults = {
        metrics: {
            title: self.getInstanceTitle(),
        },
    };

    const overlay = {
        deviceType: 'switchBinary',
        metrics: {
            icon: '',
        },
    };

    const handler = function(command, args) {
        if (command === 'on') {
            clearInterval(self.interval);
            self.interval = setInterval(function() {
                self.toggle();
            }, self.config.interval * 1000);
        } else if (command === 'off') {
            clearInterval(self.interval);
        }
    };

    this.vDev = this.controller.devices.create({
        deviceId: 'IntervalDevice_' + this.id,
        defaults: defaults,
        overlay: overlay,
        handler: handler,
        moduleId: this.id,
    });
};

IntervalDevice.prototype.stop = function() {
    if (this.vDev) {
        this.controller.devices.remove(this.vDev.id);
        this.vDev = null;
    }

    clearInterval(self.interval);

    IntervalDevice.super_.prototype.stop.call(this);
};

// ----------------------------------------------------------------------------
// --- Module methods
// ----------------------------------------------------------------------------
IntervalDevice.prototype.toggle = function() {
    this.etat = !this.etat;
    const etat = this.etat;
    const self = this;
    self.config.action.switches && self.config.action.switches.forEach(function(devState) {
        const vDev = self.controller.devices.get(devState.device);
        if (vDev) {
            const state = devState.invert ? !etat : etat;
            vDev.performCommand(state ? 'on' : 'off');
        }
    });
    self.config.action.dimmers && self.config.action.dimmers.forEach(function(devState) {
        const vDev = self.controller.devices.get(devState.device);
        if (vDev) {
            const level = etat ? devState.statusOn : devState.statusOff;
            vDev.performCommand("exact", { level: level });
        }
    });
    self.config.action.thermostats && self.config.action.thermostats.forEach(function(devState) {
        const vDev = self.controller.devices.get(devState.device);
        if (vDev) {
            const level = etat ? devState.statusOn : devState.statusOff;
            vDev.performCommand("exact", { level: level });
        }
    });
    self.config.action.locks && self.config.action.locks.forEach(function(devState) {
        const vDev = self.controller.devices.get(devState.device);
        if (vDev) {
            vDev.performCommand(etat ? devState.statusOn : devState.statusOff);
        }
    });
    self.config.action.scenes && self.config.action.scenes.forEach(function(scene) {
        const vDev = self.controller.devices.get(scene);
        if (vDev) {
            vDev.performCommand("on");
        }
    });
}
